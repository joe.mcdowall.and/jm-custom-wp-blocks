/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';

/**
 * React hook that is used to mark the block wrapper element.
 * It provides all the necessary props like the class name.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-block-editor/#useBlockProps
 */
import { InnerBlocks, useBlockProps } from '@wordpress/block-editor';

/**
 * The save function defines the way in which the different attributes should
 * be combined into the final markup, which is then serialized by the block
 * editor into `post_content`.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#save
 *
 * @return {WPElement} Element to render.
 */

export default function save( { attributes } ) {
    const blockProps = useBlockProps.save();
	const boxStyle = {
        'justify-content': attributes.alignment === 'left' ? 'flex-start' : 'flex-end'
    }
	const imageStyle = {
		backgroundImage: attributes.mediaUrl != 0 ? 'url("' + attributes.mediaUrl + '")' : 'none',
	};

    return (<div { ...blockProps } style={boxStyle}>
		<div className="copy-box">
			<span class="box-background"></span>
            <span class="box-content">
				<InnerBlocks.Content />
			</span>
		</div>
		<div className="background-image" style={imageStyle}></div>
	</div>);
}
