import { __ } from '@wordpress/i18n';
import { InnerBlocks, useBlockProps, InspectorControls, MediaUpload, MediaUploadCheck } from '@wordpress/block-editor';
import './editor.scss';
import { PanelBody, Button, ResponsiveWrapper, RadioControl, ColorPicker} from '@wordpress/components';
import { Fragment, useState } from '@wordpress/element';
import { withSelect } from '@wordpress/data';


const AlignmentControl = (props) => {
    const { attributes, setAttributes } = props;

    return (
        <RadioControl
            label="Block alignment"
            selected={ attributes.alignment || 'left' }
            options={ [
                { label: 'Left', value: 'left' },
                { label: 'Right', value: 'right' },
            ] }
            onChange={ ( value ) => {
                setAttributes( { alignment: value } );
            }}
        />
    );
}

const BoxColourControl = (props) => {
    const { attributes, setAttributes } = props;

    return (
        <ColorPicker
            label="Box background colour"
            defaultValue={ attributes.boxColour || '#000' }
            onChange={ ( value ) => {
                setAttributes( { boxColour: value } );
            }}
        />
    );
}

const BlockEdit = (props) => {
	const { attributes, setAttributes } = props;

    const onSelectMedia = (media) => {
		props.setAttributes({
			mediaId: media.id,
			mediaUrl: media.url
		});
	}

    const blockStyle = {
        'justify-content': attributes.alignment === 'left' ? 'flex-start' : 'flex-end'
    }

    const boxStyle = {
        'background-color': attributes.boxColour || '#000'
    }

    const imageStyle = {
		backgroundImage: attributes.mediaUrl != 0 ? 'url("' + attributes.mediaUrl + '")' : 'none'
	};
 
	return (
		<Fragment>
			<InspectorControls>
				<PanelBody
					title={__('Select block background image', 'jm-components')}
					initialOpen={ true }
				>
					<div className="editor-post-featured-image">
                        <MediaUploadCheck>
                            <MediaUpload
                                onSelect={onSelectMedia}
                                value={attributes.mediaId}
                                allowedTypes={ ['image'] }
                                render={({open}) => (
                                    <Button 
                                        className={attributes.mediaId == 0 ? 'editor-post-featured-image__toggle' : 'editor-post-featured-image__preview'}
                                        onClick={open}
                                    >
                                        {attributes.mediaId == 0 && __('Choose an image', 'jm')}
                                        {props.media != undefined && 
		                                    <ResponsiveWrapper
                                                naturalWidth={ props.media.media_details.width }
                                                naturalHeight={ props.media.media_details.height }
                                            >
                                                <img src={props.media.source_url} />
                                            </ResponsiveWrapper>
	}
                                    </Button>
                                )}
                            />
                        </MediaUploadCheck>
					</div>
				</PanelBody>
                <PanelBody
					title={__('Choose alignment', 'jm-components')}
					initialOpen={ true }
				>
                    <div>
                        <AlignmentControl {...props} />
                    </div>
                </PanelBody>
                <PanelBody
					title={__('Select block background image', 'jm-components')}
					initialOpen={ true }
				>
                    <div>
                        <BoxColourControl {...props} />
                    </div>
                </PanelBody>
			</InspectorControls>
			<div { ...useBlockProps() } style={blockStyle}>
                <div className="copy-box">
                    <span class="box-background" style={boxStyle}></span>
                    <span class="box-content">
                        <InnerBlocks />
                        {/* <h2> 
                            <TextControl
                            label={ __( 'Please enter a Title', 'jm' ) }
                            value={ attributes.title }
                            onChange={ ( val ) => setAttributes( { title: val } ) }
                            />
                        </h2>
                        <p>
                            <TextControl
                            label={ __( 'Please enter a paragraph', 'jm' ) }
                            value={ attributes.paragraph }
                            onChange={ ( val ) => setAttributes( { paragraph: val } ) }/>
                        </p> */}
                    </span>
                </div>
                <div className="background-image" style={imageStyle}>
                    {/* <p>Please add a background image</p> */}
                    {/* <InnerBlocks allowedBlocks={ ALLOWED_BLOCKS } /> */}
                </div>
			</div>
		</Fragment>
	);
};

const Edit = withSelect((select, props) => {
	return { media: props.attributes.mediaId ? select('core').getMedia(props.attributes.mediaId) : undefined };
})(BlockEdit);

export default Edit;