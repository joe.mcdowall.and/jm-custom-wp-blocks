<?php
/**
 * Plugin Name:       JM Banner
 * Description:       A simple banner element.
 * Requires at least: 5.8
 * Requires PHP:      7.0
 * Version:           0.1.0
 * Author:            Joe McDowall
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       jm-banner
 *
 * @package           create-block
 */

/**
 * Registers the block using the metadata loaded from the `block.json` file.
 * Behind the scenes, it registers also all assets so they can be enqueued
 * through the block editor in the corresponding context.
 *
 * @see https://developer.wordpress.org/reference/functions/register_block_type/
 */
function create_block_jm_banner_block_init() {
	register_block_type( __DIR__ . '/build' );
}
add_action( 'init', 'create_block_jm_banner_block_init' );

function jm_banner_plugin_block_categories( $categories ) {
    return array_merge(
        $categories,
        [
            [
                'slug'  => 'jm-components',
                'title' => __( 'JM Components', 'jm-components-boilerplate' ),
            ],
        ]
    );
}
add_action( 'block_categories_all', 'jm_banner_plugin_block_categories', 10, 2 );