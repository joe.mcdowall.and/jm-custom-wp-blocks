<?php
/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package jm-block-theme
 * @since 1.0.0
 */

/**
 * Enqueue the style.css file.
 * 
 * @since 1.0.0
 */
function jm_block_theme_styles() {
	wp_enqueue_style(
		'jm_block_theme-style',
		get_stylesheet_uri(),
		wp_get_theme()->get( 'Version' )
	);

}
add_action( 'wp_enqueue_scripts', 'jm_block_theme_styles' );

if ( ! function_exists( 'jm_block_theme_setup' ) ) {
	function jm_block_theme_setup() {
		add_theme_support( 'wp-block-styles' );
	}
}
add_action( 'after_setup_theme', 'jm_block_theme_setup' );

?>
